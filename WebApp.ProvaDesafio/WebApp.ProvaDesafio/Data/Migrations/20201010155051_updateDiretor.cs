﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.ProvaDesafio.Data.Migrations
{
    public partial class updateDiretor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilmeId",
                table: "Diretores");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "FilmeId",
                table: "Diretores",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
