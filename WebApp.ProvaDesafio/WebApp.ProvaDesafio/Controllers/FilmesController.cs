﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.ProvaDesafio.Data;
using WebApp.ProvaDesafio.Models;
using WebApp.ProvaDesafio.Models.Enums;

namespace WebApp.ProvaDesafio.Controllers
{
    public class FilmesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FilmesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Filmes
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Filmes.Include(f => f.Ator).Include(f => f.Diretor);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Filmes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var filme = await _context.Filmes
                .Include(f => f.Ator)
                .Include(f => f.Diretor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (filme == null)
            {
                return NotFound();
            }

            return View(filme);
        }

        // GET: Filmes/Create
        public IActionResult Create()
        {
            ViewData["AtorId"] = new SelectList(_context.Atores, "Id", "Nome");
            ViewData["DiretorId"] = new SelectList(_context.Diretores, "Id", "Nome");

            return View();
        }

        // POST: Filmes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Nome,Ativo,Genero,Voto,MediaVoto,DiretorId,AtorId,Id")] Filme filme)
        {
            if (ModelState.IsValid)
            {
                filme.Id = Guid.NewGuid();
                _context.Add(filme);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AtorId"] = new SelectList(_context.Atores, "Id", "Nome", filme.AtorId);
            ViewData["DiretorId"] = new SelectList(_context.Diretores, "Id", "Nome", filme.DiretorId);
            return View(filme);
        }

        // GET: Filmes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var filme = await _context.Filmes.FindAsync(id);
            if (filme == null)
            {
                return NotFound();
            }
            ViewData["AtorId"] = new SelectList(_context.Atores, "Id", "Nome", filme.AtorId);
            ViewData["DiretorId"] = new SelectList(_context.Diretores, "Id", "Nome", filme.DiretorId);
            return View(filme);
        }

        // POST: Filmes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Nome,Ativo,Genero,Voto,MediaVoto,DiretorId,AtorId,Id")] Filme filme)
        {
            if (id != filme.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(filme);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FilmeExists(filme.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AtorId"] = new SelectList(_context.Atores, "Id", "Nome", filme.AtorId);
            ViewData["DiretorId"] = new SelectList(_context.Diretores, "Id", "Nome", filme.DiretorId);
            return View(filme);
        }

        // GET: Filmes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var filme = await _context.Filmes
                .Include(f => f.Ator)
                .Include(f => f.Diretor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (filme == null)
            {
                return NotFound();
            }

            return View(filme);
        }

        // POST: Filmes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var filme = await _context.Filmes.FindAsync(id);
            _context.Filmes.Remove(filme);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FilmeExists(Guid id)
        {
            return _context.Filmes.Any(e => e.Id == id);
        }
    }
}
