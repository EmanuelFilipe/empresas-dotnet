﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ProvaDesafio.Models
{
    public class Ator : Entity
    {
        [Required]
        public string Nome { get; set; }

        [DisplayName("Ativo?")]
        public bool Ativo { get; set; }

        //public Guid FilmesId { get; set; }
        public IEnumerable<Filme> Filmes { get; set; }
    }
}
