﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ProvaDesafio.Models.Enums;

namespace WebApp.ProvaDesafio.Models
{
    public class Filme : Entity
    {
        [Required]
        public string Nome { get; set; }

        [DisplayName("Ativo?")]
        public bool Ativo { get; set; }

        public Genero Genero { get; set; }
        public int Voto { get; set; }

        [DisplayName("Média Voto")]
        public double MediaVoto { get; set; }

        public Guid DiretorId { get; set; }
        public Diretor Diretor { get; set; }

        public Guid AtorId { get; set; }
        public Ator Ator { get; set; }

    }
}
