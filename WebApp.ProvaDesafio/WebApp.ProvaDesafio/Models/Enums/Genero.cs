﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ProvaDesafio.Models.Enums
{
    public enum Genero
    {
        Acao,
        Ficcao,
        Romance,
        Comedia
    }
}
