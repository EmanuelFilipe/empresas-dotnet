﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ProvaDesafio.Models.Enums;

namespace WebApp.ProvaDesafio.Models
{
    public class Usuario : Entity
    {
        [Required]
        public string Nome { get; set; }

        [DisplayName("Ativo ?")]
        public bool Ativo { get; set; }

        [DisplayName("Tipo Usuário")]
        public TipoUsuario TipoUsuario { get; set; }
    }
}
