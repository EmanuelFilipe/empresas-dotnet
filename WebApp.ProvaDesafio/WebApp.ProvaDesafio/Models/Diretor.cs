﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ProvaDesafio.Models
{
    public class Diretor : Entity
    {
        [Required]
        public string Nome { get; set; }

        [DisplayName("Ativo ?")]
        public bool Ativo { get; set; }

        //public Guid FilmeId { get; set; }
        //public ICollection<Filme> Filmes { get; set; }
    }
}
